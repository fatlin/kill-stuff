﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeaponEvent.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Logic.Features.Weapons.Events
{
    using Slash.ECS.Events;

    [GameEventType]
    public enum WeaponEvent
    {
        /// <summary>
        ///   A weapon fired a bullet.
        ///   <para>
        ///     Data: <see cref="WeaponFiredData" />
        ///   </para>
        /// </summary>
        Fired,

        Reloaded,
    }
}