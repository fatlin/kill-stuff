﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GameRootContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Unity.Features.Core.Contexts
{
    using KillStuff.Unity.Features.Weapons.Contexts;

    using Slash.ECS;
    using Slash.Unity.Common.ECS;

    using UnityEngine;

    public class GameRootContext : MonoBehaviour
    {
        #region Fields

        public GameBehaviour GameBehaviour;

        #endregion

        #region Properties

        public WeaponContext Weapon { get; set; }

        #endregion

        #region Methods

        protected void OnDisable()
        {
            this.GameBehaviour.GameChanged -= this.OnGameChanged;
        }

        protected void OnEnable()
        {
            this.GameBehaviour.GameChanged += this.OnGameChanged;
            var game = this.GameBehaviour.Game;
            if (game != null)
            {
                this.CreateContexts(game);
            }
        }

        private void CreateContexts(Game game)
        {
            this.Weapon = new WeaponContext();
            this.Weapon.Init(game.EventManager, game.EntityManager);
        }

        private void OnGameChanged(Game newGame, Game oldGame)
        {
            this.CreateContexts(newGame);
        }

        #endregion
    }
}