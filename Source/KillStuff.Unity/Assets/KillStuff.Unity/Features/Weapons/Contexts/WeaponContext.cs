﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WeaponContext.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace KillStuff.Unity.Features.Weapons.Contexts
{
    using KillStuff.Logic.Features.Core.Events;
    using KillStuff.Logic.Features.Weapons.Components;
    using KillStuff.Logic.Features.Weapons.Events;
    using KillStuff.Unity.Features.Core.Contexts;

    using Slash.ECS.Events;
    using Slash.Unity.DataBind.Core.Data;

    public class WeaponContext : GameContext
    {
        #region Fields

        private readonly Property<int> currentBulletCountProperty = new Property<int>();

        private readonly Property<int> maxBulletCountProperty = new Property<int>();

        private WeaponComponent weaponComponent;

        #endregion

        #region Properties

        public int CurrentBulletCount
        {
            get
            {
                return this.currentBulletCountProperty.Value;
            }
            set
            {
                this.currentBulletCountProperty.Value = value;
            }
        }

        public int MaxBulletCount
        {
            get
            {
                return this.maxBulletCountProperty.Value;
            }
            set
            {
                this.maxBulletCountProperty.Value = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        public override void SetEventListeners()
        {
            this.SetEventListener(CoreEvent.PlayerCreated, this.OnPlayerCreated);
            this.SetEventListener(WeaponEvent.Fired, this.OnWeaponFired);
            this.SetEventListener(WeaponEvent.Reloaded, this.OnWeaponReloaded);
        }

        #endregion

        #region Methods

        private void OnPlayerCreated(GameEvent e)
        {
            var playerEntityId = (int)e.EventData;

            // Get components of player that contain data.
            this.weaponComponent = this.EntityManager.GetComponent<WeaponComponent>(playerEntityId);

            // Update data.
            this.UpdateBulletCounts();
        }

        private void OnWeaponFired(GameEvent e)
        {
            // Update data.
            this.UpdateBulletCounts();
        }

        private void OnWeaponReloaded(GameEvent e)
        {
            // Update data.
            this.UpdateBulletCounts();
        }

        private void UpdateBulletCounts()
        {
            this.CurrentBulletCount = this.weaponComponent != null ? this.weaponComponent.BulletCount : 0;
            this.MaxBulletCount = this.weaponComponent != null ? this.weaponComponent.MaxBulletCount : 0;
        }

        #endregion
    }
}